<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    public function specificFunction(): string
    {
        // Votre logique spécifique du Module1 ici
        return 'Résultat de la fonction spécifique du Module TAS';
    }
}
